/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.Controller;

import cl.Trabajo2.dao.ClientesJpaController;
import cl.Trabajo2.dao.exceptions.NonexistentEntityException;
import cl.Trabajo2.entity.Clientes;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author luisg
 */
@WebServlet(name = "LoginController", urlPatterns = {"/LoginController"})
public class LoginController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try (PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet InsertController</title>");            
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet InsertController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
           //processRequest(request, response);
      String rut = request.getParameter("rut");
        int tipo = Integer.parseInt(request.getParameter("tipo"));
        System.out.print(rut);
       ClientesJpaController daoClientes = new ClientesJpaController();
       System.out.print(tipo);
       if (tipo == 1) {
          try {
            daoClientes.destroy(rut);
        } catch (NonexistentEntityException ex) {
            Logger.getLogger(EliminarController.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.getRequestDispatcher("ListarController").forward(request, response);  
       }
       else{
           Clientes datosClientes = daoClientes.findClientes(rut);
            request.setAttribute("datosClientes", datosClientes);
            request.getRequestDispatcher("Editar.jsp").forward(request, response);


       }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
      String nombre=request.getParameter("nombre");
        String apellidos=request.getParameter("apellidos");
        String rut=request.getParameter("rut");
        String nacimiento=request.getParameter("nacimiento");
        String email=request.getParameter("email");
        
        Clientes cliente=new Clientes();
        cliente.setNombre(nombre);
        cliente.setApellidos(apellidos);
        cliente.setRut(rut);
        cliente.setNacimiento(nacimiento);
        cliente.setEmail(email);
        
        ClientesJpaController dao=new ClientesJpaController();
     
        try {
            dao.create(cliente);
            
    
        } catch (Exception ex) {
            Logger.getLogger(LoginController.class.getName()).log(Level.SEVERE, null, ex);
        }
        System.out.print("prueba");
        response.sendRedirect("ListarController");
        //request.getRequestDispatcher("ListarController").forward(request, response);
    }
    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
