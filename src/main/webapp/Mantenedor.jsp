<%-- 
    Document   : VistaDatos
    Created on : 29-05-2021, 20:20:06
    Author     : luisg
--%>

<%@page import="java.util.Iterator"%>
<%@page import="java.util.List"%>
<%@page import="cl.Trabajo2.entity.Clientes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    List<Clientes> lista = (List<Clientes>) request.getAttribute("clientes");
    Iterator<Clientes> itClientes = lista.iterator();
%>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>

        <section id="about" class="content">
            <h2>Registro de Persona</h2>
            <p>
            <form  name="form" action="MantenerAcciones" method="POST">


                <table border="1">
                    <thead>
                    <th>Nombre</th>
                    <th>Apellidos</th>
                    <th>Rut</th>
                    <th>Fecha Nacimiento</th>
                    <th>Email</th>
                    <th>Edición</th>
                    </thead>
                    <tbody>
                        <%while (itClientes.hasNext()) {
                                        Clientes cliente = itClientes.next();%>
                        <tr>
                            <td><%=cliente.getNombre()%></td>
                            <td><%=cliente.getApellidos()%></td>
                            <td><a href="LoginController?rut=<%=cliente.getRut()%>&tipo=1" title="Eliminar Registro"><%=cliente.getRut()%></a></td>
                            <td><%=cliente.getNacimiento()%></td>
                            <td><%=cliente.getEmail()%></td>
                            <td><a href="LoginController?rut=<%=cliente.getRut()%>&tipo=2">Editar</a></td>
                        </tr>
                        <%}%>
                    </tbody>
                    <!--                           
                                               <button type="submit" name="accion" value="agregar" class="btn btn-success">Agregar</button>
                                               <button type="submit" name="accion" value="eliminar" class="btn btn-success">Eliminar</button>
                                               <button type="submit" name="accion" value="actualizar" class="btn btn-success">Actualizar</button> //-->
                </table>

            </form>
        </p>
    </section>
</body>
</html>
