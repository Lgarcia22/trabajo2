<%-- 
    Document   : Editar
    Created on : 06-06-2021, 18:29:06
    Author     : luisg
--%>

<%@page import="cl.Trabajo2.entity.Clientes"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%  Clientes datosClientes = (Clientes) request.getAttribute("datosClientes");
    if (datosClientes == null) {
        response.sendRedirect("ListarController");
    }%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h2>Editar Persona</h2>
        <form action="ListarController" method="POST">         
  <p>Nombre: <br><input type="text" name="nombre" size="20" value="<%= datosClientes.getNombre() %>"></p>
  <p>Apellidos: <br><input type="text" name="apellidos" min="1900" value="<%= datosClientes.getApellidos() %>"></p>
  <p>Rut: <br><input type="number" name="rut" min="1900" value="<%= datosClientes.getRut() %>" readonly="readonly"></p> 
  <p>Fecha de nacimiento: <br><input type="date" name="nacimiento" min="1900" value="<%= datosClientes.getNacimiento() %>"></p> 
  <p>Email: <br><input type="text" name="email" min="1900" value="<%= datosClientes.getEmail() %>"></p> 
  <p>
      <button type="submit" name="accion" value="ingresar">Editar</button>
  </p>
</form>
    </body>
</html>
